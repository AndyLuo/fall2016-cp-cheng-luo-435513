import angular from 'angular';
import angularMeteor from 'angular-meteor';
import { Discussions } from '../imports/api/discussions';
import { Follows } from '../imports/api/follows';
import { Personals } from '../imports/api/personals';
import { name as Share } from '../imports/ui/components/share/share';