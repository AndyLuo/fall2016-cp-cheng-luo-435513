import { Mongo } from 'meteor/mongo';
export const Discussions = new Mongo.Collection('discussions');

Discussions.allow({ //setting permission for database actions
  insert(userId, discussion) {
    return userId && discussion.userId === userId;
  },
  update() {
  	return true;
  },
  remove(userId, discussion) {
  	return userId && discussion.userId === userId;
  },
});