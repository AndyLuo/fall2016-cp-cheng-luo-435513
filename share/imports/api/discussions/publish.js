import { Meteor } from 'meteor/meteor';
 
import { Discussions } from './collection';
 
if (Meteor.isServer) {
  Meteor.publish('discussions', function() {
    return Discussions.find({});
  });
}