import { Mongo } from 'meteor/mongo';
export const Files = new FS.Collection("files", {
  stores: [new FS.Store.FileSystem("files", {path: "~/uploads"})]
});

Files.allow({
  insert() {
    return true;
  },
  update() {
    return true;
  },
  download() {
    return true;
  }
});