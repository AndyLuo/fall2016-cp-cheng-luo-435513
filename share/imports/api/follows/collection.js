import { Mongo } from 'meteor/mongo';
export const Follows = new Mongo.Collection('follows');

Follows.allow({ //setting permission for database actions
  insert(userId, follow) {
    return userId && follow.userId === userId;
  },
  update() {
    return true;
  },
  remove(userId, follow) {
  	return userId && follow.userId === userId;
  },
});