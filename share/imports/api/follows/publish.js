import { Meteor } from 'meteor/meteor';
 
import { Follows } from './collection';
 
if (Meteor.isServer) {
  Meteor.publish('follows', function() {
    return Follows.find({});
  });
}