import { Mongo } from 'meteor/mongo';
export const Personals = new Mongo.Collection('personals');

Personals.allow({ //setting permission for database actions
  insert(userId, personal) {
    return userId && personal.userId === userId;
  },
  update(userId, personal, fields, modifier) {
    return userId && personal.userId === userId;
  }
});