import { Meteor } from 'meteor/meteor';
 
import { Personals } from './collection';
 
if (Meteor.isServer) {
  Meteor.publish('personals', function() {
    return Personals.find({});
  });
}