import { Meteor } from 'meteor/meteor';
 
const USER_FIELDS = {
  _id: 1,
  emails: 1,
};
Meteor.users.allow({ //setting permission for database actions
	update() {
		return true;
	}
});
if (Meteor.isServer) {
  Meteor.publish("users", function () {
  	return Meteor.users.find();
  });
}