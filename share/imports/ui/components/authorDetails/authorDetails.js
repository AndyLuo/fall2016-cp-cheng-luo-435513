import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';
import { Discussions } from '../../../api/discussions';
import { Personals } from '../../../api/personals';
import { Follows } from '../../../api/follows';
import { name as Navigation } from '../navigation/navigation';
import { name as DiscussionAdd } from '../discussionAdd/discussionAdd';
import template from './authorDetails.html';
 
class AuthorDetails {
  constructor($stateParams, $scope, $reactive) {
    'ngInject';
    document.getElementById("descript_form").style.display = "none";
    $reactive(this).attach($scope);
    this.newPersonal = {};
    this.authorId = $stateParams.authorId;
    this.orderProp = 'like';
    this.reverse = true;
    this.subscribe('users');
    this.subscribe('discussions');
    this.subscribe('personals');
    this.subscribe('follows');
 
    this.helpers({ //defining variables used in this template
      author() {
        return Meteor.users.findOne(this.authorId);
      },

      discussions() {
        return Discussions.find({"userId" : this.authorId});
      },

      ownFollows() {
        return Follows.find({"userId" : this.authorId});
      },

      isOwn() {
        return this.authorId === Meteor.userId();
      },

      hasPersonal() {
        if (Personals.find({"userId" : this.authorId}).count() === 0) {
          return false;
        }
        else {
          return true;
        }
      },

      personal() {
        return Personals.findOne({"userId" : this.authorId});
      },

    });
  }

  changeOrder() { //setting the order to reverse
    if (this.orderProp === "createdAt" || this.orderProp === "like" || this.orderProp === "follow") {
      this.reverse = true;
    }
    else this.reverse = false;
  }

  findDiscussionTitle(discussionId) { //find discussion title according to discussion id
    var thisDiscussion = Discussions.findOne({"_id" : discussionId});
    var thisDiscussionTitle = thisDiscussion.title;
    return thisDiscussionTitle;
  }

  showEdit() { //change view of edit description form
    if (document.getElementById("descript_form").style.display === "none") {
      document.getElementById("descript_form").style.display = "block";
      $('#descript_input').focus();
    }
    else {
      document.getElementById("descript_form").style.display = "none";
    }
  }
  descriptSubmit() { //handling submitting of description form
    if (typeof this.newPersonal.descript !== 'undefined') {
      this.newPersonal.userId = Meteor.userId();
      this.newPersonal.user = Meteor.user().emails[0].address;
      this.newPersonal.like = 0;
      if (Personals.find({"userId" : this.authorId}).count() === 0) {
        Personals.insert(this.newPersonal);
      }
      else {
        Personals.update({"_id" : this.personal._id}, {$set: {"descript" : this.newPersonal.descript}});
      }
      this.reset();
    }
    else {
      alert("Please input a valid description!");
    }
  }

  like(discussionId) { //handling like button clicking
    Discussions.update({"_id" : discussionId}, {$inc: {like : 1}});
  }

  deleteDiscussion(discussionId) { //handling deleting discussions
    Discussions.remove({"_id" : discussionId});
    while (Follows.find({"discussionId" : discussionId}).count() !== 0) {
      var thisFollow = Follows.findOne({"discussionId" : discussionId});
      Follows.remove({"_id" : thisFollow._id});
    }
  }

  reset() { //reset this.newPersonal
    this.newPersonal = {};
    document.getElementById("descript_form").style.display = "none";
  }

}
 
const name = 'authorDetails';
 
// create a module
export default angular.module(name, [
  angularMeteor,
  uiRouter,
  Navigation,
  DiscussionAdd,
]).component(name, {
  template,
  controllerAs: name,
  controller: AuthorDetails
})
.config(config);
 
function config($stateProvider) {
  'ngInject';
 
  $stateProvider.state('authorDetails', {
    url: '/authors/:authorId',
    template: '<author-details></author-details>',
    resolve: {
      currentUser($q) {
        if (Meteor.userId() === null) {
          return $q.reject('AUTH_REQUIRED');
        } else {
          return $q.resolve();
        }
      }
    }
  });
}
