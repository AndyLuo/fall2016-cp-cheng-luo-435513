import angular from 'angular';
import angularMeteor from 'angular-meteor';
import template from './discussion.html';
import uiRouter from 'angular-ui-router';
import { Discussions } from '../../../api/discussions';
import { Follows } from '../../../api/follows';
import { name as Navigation } from '../navigation/navigation';

class Discussion {
  constructor($stateParams, $scope, $reactive, $state) {
    'ngInject';
    this.follow = {};
    $reactive(this).attach($scope);
    this.discussionId = $stateParams.discussionId;
    this.state = $state;
    this.subscribe('discussions');
    this.subscribe('follows');
    document.getElementById("edit_title_form").style.display = "none";
    document.getElementById("edit_content_form").style.display = "none";
    $("html, body").animate({ scrollTop: $(document).height() }, "slow");
    this.helpers({ //defining local variables used in this template
      discussion() {
        return Discussions.findOne({"_id" : this.discussionId});
      },
      isOwn() {
        var d = Discussions.findOne({"_id" : this.discussionId});
        if (typeof d !== 'undefined') {
          return d.userId === Meteor.userId();
        }
      },
      follows() {
        return Follows.find({"discussionId" : this.discussionId});
      },
    }); 
  }

  hasFollow() { //determining whether a discussion has any followups
    return Follows.find({"discussionId" : this.discussionId}).count() !== 0;
  }

  isOwnFollow(followId) { //determining if the discussion is this user's
    var thisFollow = Follows.findOne({"_id" : followId});
    var thisFollowUserId = thisFollow.userId;
    return thisFollowUserId === Meteor.userId();
  }

  deleteFollow(followId) { //handling deleting followups
    Follows.remove({"_id" : followId});
  }

  showEditTitle() { //controlling view of edit title form
    if (document.getElementById("edit_title_form").style.display === "none") {
      document.getElementById("edit_title_form").style.display = "block";
      $('#edit_title_form_input').focus();
    }
    else document.getElementById("edit_title_form").style.display = "none";
  }

  showEditContent() { //controlling view of edit content form
    if (document.getElementById("edit_content_form").style.display === "none") {
      document.getElementById("edit_content_form").style.display = "block";
      $('#edit_content_form_input').focus();
    }
    else document.getElementById("edit_content_form").style.display = "none";
  }

  deleteDiscussion() { //handling deleting discussions
    Discussions.remove({"_id" : this.discussionId});
    this.state.go("plaza");
  }

  submitTitle() { //handling change titles
    if (typeof this.newDiscussion !== "undefined") {
      Discussions.update({"_id" : this.discussionId}, {$set: {"title" : this.newDiscussion.title}});
      this.resetNewDiscussion();
    }
    else {
      alert("Please fill in a valid title!");
    }
  }

  submitContent() { //handling change content
    if (typeof this.newDiscussion !== "undefined") {
      Discussions.update({"_id" : this.discussionId}, {$set: {"content" : this.newDiscussion.content}});
      this.resetNewDiscussion();
    }
    else {
      alert("Please fill in valid content!");
    }
  }

  submit() { //handling submit of followups
    if (typeof this.follow.content !== 'undefined') {
      this.follow.userId = Meteor.userId();
      this.follow.author = Meteor.user().emails[0].address;
      this.follow.discussionId = this.discussionId;
      Follows.insert(this.follow);
      Discussions.update({"_id" : this.discussionId}, {$inc: {follow : 1}});
      this.reset();
      $("html, body").animate({ scrollTop: $(document).height() }, "slow");
    }
    else {
      alert("Please fill in a valid followup!");
    }
  }

  reset() { //reset this.follow
    this.follow = {};
  }

  resetNewDiscussion() { //reset this.newDiscussion
    document.getElementById("edit_title_form").style.display = "none";
    document.getElementById("edit_content_form").style.display = "none";
    this.newDiscussion = {};
  }
}

const name = 'discussion';

// create a module
export default angular.module(name, [
  angularMeteor,
  uiRouter,
  Navigation
]).component(name, {
  template,
  controllerAs: name,
  controller: Discussion
}).config(config);
 
function config($stateProvider) {
  'ngInject';
 
  $stateProvider.state('discussion', {
    url: '/discussion/:discussionId',
    template: '<discussion></discussion>',
    resolve: {
      currentUser($q) {
        if (Meteor.userId() === null) {
          return $q.reject('AUTH_REQUIRED');
        } else {
          return $q.resolve();
        }
      }
    }
  });
}