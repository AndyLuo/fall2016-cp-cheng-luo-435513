import angular from 'angular';
import angularMeteor from 'angular-meteor';
import { Discussions } from '../../../api/discussions';
import { Meteor } from 'meteor/meteor';
import template from './discussionAdd.html';
 
class DiscussionAdd {
  constructor() {
    this.discussion = {};
    $('#discussion_add_title').focus();
  }
 
  submit() { //handling submission of a discussion
    if (typeof this.discussion.title == 'undefined' || typeof this.discussion.content == 'undefined') {
      alert("Please fill in both title and content to submit!");
      return;
    }
    else {
      console.log(this.discussion.title);
      console.log(this.discussion.content);
      this.discussion.userId = Meteor.userId();
      this.discussion.author = Meteor.user().emails[0].address;
      this.discussion.like = 0;
      this.discussion.follow = 0;
      this.discussion.createdAt = new Date();
      Discussions.insert(this.discussion);

      this.reset();
      alert("Submission is Successful!");
      $('#discussion_add_title').focus();
    }
  }

  reset() { //reset this.discussion
    this.discussion = {};
  }

}
 
const name = 'discussionAdd';
 
// create a module
export default angular.module(name, [
  angularMeteor
]).component(name, {
  template,
  controllerAs: name,
  controller: DiscussionAdd
});