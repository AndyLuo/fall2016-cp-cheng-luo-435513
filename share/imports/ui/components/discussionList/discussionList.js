import angular from 'angular';
import angularMeteor from 'angular-meteor';
import template from './discussionList.html';
import uiRouter from 'angular-ui-router';
import { Discussions } from '../../../api/discussions';
import { name as DiscussionAdd } from '../discussionAdd/discussionAdd';
import { name as Navigation } from '../navigation/navigation';

class DiscussionList {
  constructor($scope, $reactive) {
    'ngInject';

    $reactive(this).attach($scope);

    this.orderProp = 'like';
    this.reverse = true;

    this.subscribe('discussions');

    this.showAdd = false;
    
    this.helpers({ //defining local variables used in this template
      discussions() {
          return Discussions.find({});
      },
    }); 
  }

  changeOrder() { //set the order to be reverse if time, follow or like is selected
    if (this.orderProp === "createdAt" || this.orderProp === "like" || this.orderProp === "follow") {
      this.reverse = true;
    }
    else this.reverse = false;
  }

  like(discussionId) { //handling like button clicking
    Discussions.update({"_id" : discussionId}, {$inc: {like : 1}});
  }

  changeShowAdd() { //change view of add form
    this.showAdd = !this.showAdd;
  }
}

const name = 'discussionList';

// create a module
export default angular.module(name, [
  angularMeteor,
  uiRouter,
  DiscussionAdd
]).component(name, {
  template,
  controllerAs: name,
  controller: DiscussionList
});
// .config(config);
 
// function config($stateProvider) {
//   'ngInject';
//   $stateProvider
//     .state('plaza', {
//       url: '/plaza',
//       template: '<music-list></music-list>'
//     });
// }