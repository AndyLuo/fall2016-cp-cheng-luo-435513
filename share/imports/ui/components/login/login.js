import angular from 'angular';
import angularMeteor from 'angular-meteor';
import template from './login.html';
import uiRouter from 'angular-ui-router';
import { Meteor } from 'meteor/meteor';
import { Discussions } from '../../../api/discussions';
import { Follows } from '../../../api/follows';

class Login {
  constructor($scope, $reactive) {
    'ngInject';

    $reactive(this).attach($scope);
    this.subscribe('discussions');
    this.subscribe('follows');
    this.subscribe('users');

    this.helpers({ //defining local variables used in this template
      numberOfDiscussions() {
        return Discussions.find().count();
      },

      numberOfFollows() {
        return Follows.find().count();
      },

      numberOfUsers() {
        console.log(Meteor.users.find().count());
        return Meteor.users.find().count();
      },
    });
  }

  login() { //handling submission of log in form
    event.preventDefault();
    var email = document.getElementById('login_email').value;
    var password = document.getElementById('login_password').value;
    Meteor.loginWithPassword(email, password);
  } 

  signup() { //handling submission of sign up form
    event.preventDefault();
    var email = document.getElementById('login_email').value;
    var password = document.getElementById('login_password').value;
    Accounts.createUser({
      email: email,
      password: password,
      profile: {
        level: 0,
        point: 0
      }
    });
  }

} 

const name = 'login';

// create a module
export default angular.module(name, [
  angularMeteor,
  uiRouter
]).component(name, {
  template,
  controllerAs: name,
  controller: Login
})
.config(config);
 
function config($stateProvider) {
  'ngInject';
  $stateProvider
    .state('login', {
      url: '/login',
      template: '<login></login>'
    });
}
