import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';
import { Discussions } from '../../../api/discussions';
import { Follows } from '../../../api/follows';
import { Personals } from '../../../api/personals';
import { Meteor } from 'meteor/meteor';
import template from './navigation.html';
 

class Navigation {
  constructor($scope, $reactive) {
    'ngInject'; 
    $reactive(this).attach($scope);

    this.subscribe("discussions");
    this.subscribe("follows");
    this.subscribe("personals");
    this.subscribe('users');
    this.helpers({ //defining local variables used in this template
      user() {
        return Meteor.userId();
      },
      logIn() {
        return Meteor.user();
      },
      userName() {
        if (Meteor.user()) {
          return Meteor.user().emails[0].address;
        }
        else {
          return;
        }
      }
    });

  }

  calculatePoints() { 
  //calculating a user's current point, 1 point for followup, 3 points for discussion and 10 points bonus if description is added
    if (Meteor.user()) {
      var numFollows = Follows.find({"userId" : Meteor.userId()}).count();
      var numDiscussions = Discussions.find({"userId" : Meteor.userId()}).count();
      var hasDescript = (Personals.find({"userId" : Meteor.userId()}).count() !== 0 );
      var numDescript = 0;
      if (hasDescript) {
        numDescript = 10;
      }
      var points = 1*numFollows + 3*numDiscussions + numDescript;
      console.log("Current Point is: "+points);
      Meteor.users.update(Meteor.userId(), {$set: {"profile.point" : points}});
      return points;
    }
    else return;
  }

  userLevel() { //determing a user's level
    if (Meteor.user()) {
      var points = this.calculatePoints();
      if (points < 25) {
        // return 0;
        Meteor.users.update(Meteor.userId(), {$set: {"profile.level" : 0}});
      }
      else if (points < 50) {
        // return 1;
        Meteor.users.update(Meteor.userId(), {$set: {"profile.level" : 1}});
      }
      else if (points < 100) {
        // return 2;
        Meteor.users.update(Meteor.userId(), {$set: {"profile.level" : 2}});
      }
      else if (points < 200) {
        // return 3;
        Meteor.users.update(Meteor.userId(), {$set: {"profile.level" : 3}});
      }
      else if (points < 500) {
        // return 4;
        Meteor.users.update(Meteor.userId(), {$set: {"profile.level" : 4}});
      }
      else if (points < 1000) {
        Meteor.users.update(Meteor.userId(), {$set: {"profile.level" : 5}});
      }
      else if (points < 2000) {
        Meteor.users.update(Meteor.userId(), {$set: {"profile.level" : 6}});
      }
      return Meteor.user().profile.level;
    }
    else {
      return;
    }
  }

  logout($rootScope, $state) { //handling logout
    event.preventDefault();
    Meteor.logout();
  }
}
const name = 'navigation';
 
// create a module
export default angular.module(name, [
  angularMeteor,
  uiRouter
]).component(name, {
  template,
  controllerAs: name,
  controller: Navigation
});