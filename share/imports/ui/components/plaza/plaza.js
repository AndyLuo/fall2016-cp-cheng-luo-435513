import angular from 'angular';
import angularMeteor from 'angular-meteor';
import template from './plaza.html';
import uiRouter from 'angular-ui-router';
import { name as Navigation } from '../navigation/navigation';
import { name as DiscussionList } from '../discussionList/discussionList';
import { Meteor } from 'meteor/meteor';
import { Discussions } from '../../../api/discussions';

class Plaza {
  constructor($scope, $reactive) {
    'ngInject';

    $reactive(this).attach($scope);
  }
}

const name = 'plaza';

// create a module
export default angular.module(name, [
  angularMeteor,
  uiRouter,
  DiscussionList,
  Navigation
]).component(name, {
  template,
  controllerAs: name,
  controller: Plaza
})
.config(config);
 
function config($stateProvider) {
  'ngInject';
  $stateProvider
    .state('plaza', {
      url: '/plaza',
      template: '<plaza></plaza>',
      resolve: {
        currentUser($q) {
          if (Meteor.userId() === null) {
            return $q.reject('AUTH_REQUIRED');
          } else {
            return $q.resolve();
          }
        }
      }
    });
}