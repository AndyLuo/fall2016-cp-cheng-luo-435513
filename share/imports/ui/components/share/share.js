import angular from 'angular';
import angularMeteor from 'angular-meteor';
 
import template from './share.html';
import { name as Navigation } from '../navigation/navigation';
import { name as Login } from '../login/login';
import { name as Plaza } from '../plaza/plaza';
import { name as AuthorDetails } from '../authorDetails/authorDetails';
import uiRouter from 'angular-ui-router';
import { Meteor } from 'meteor/meteor';
import { name as DiscussionList } from '../discussionList/discussionList';
import { name as Discussion } from '../discussion/discussion';
 
class Share {}
 
const name = 'share';
 
// create a module
export default angular.module(name, [
  angularMeteor,
  uiRouter,
  DiscussionList,
  Navigation,
  AuthorDetails,
  Plaza,
  Login,
  Discussion,
  'accounts.ui'
]).component(name, {
  template,
  controllerAs: name,
  controller: Share
})
  .config(config)
  .run(run);

function config($locationProvider, $urlRouterProvider) {
  'ngInject';

  $locationProvider.html5Mode(true);
  $urlRouterProvider
  .otherwise('/login');
}

function run($rootScope, $state) {
  'ngInject';
 
  $rootScope.$on('$stateChangeError',
    (event, toState, toParams, fromState, fromParams, error) => {
      if (error === 'AUTH_REQUIRED') {
        $state.go('login');
      }
    }
  );

  Accounts.onLogin(function () { //change view to plaza if log in is success
    if ($state.is('login')) {
      if (Meteor.user()) {
        $state.go('plaza');
      }
    }
  });

  Accounts.onLoginFailure(function () { //alert the user if login failed
    $state.go('login');
    alert("Login Failed: Email address or password is incorrect! OR This Email has already registered!");
  });

  Accounts.onLogout(function () { //go to login page after logout
    $state.go('login');
  });
}