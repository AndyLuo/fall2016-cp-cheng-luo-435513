import { Meteor } from 'meteor/meteor';
import '../imports/api/discussions';
import '../imports/api/follows';
import '../imports/api/users';
import '../imports/api/personals';

Meteor.startup(() => {});